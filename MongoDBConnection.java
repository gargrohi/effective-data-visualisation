package com.MongoDBConn;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.BSONObject;

import com.mongodb.*;

public class MongoDBConnection {
	
	private  static MongoClientURI uri=null;
	private  static MongoClient client=null;
	private static DB db=null;
	private DBCollection collection=null;
	
	public  MongoDBConnection() throws UnknownHostException{
		
	
		if(uri==null){
			uri= new MongoClientURI("mongodb://rohitgarg:p55word@ds029630.mongolab.com:29630/geodata"); 
			client= new MongoClient(uri);
		    db = client.getDB(uri.getDatabase());
		    collection = db.getCollection("geoColl");
		}
	    
	}
	public void insertData(DBObject[] seedData)
	{
		collection.insert(seedData);
		System.out.println("Inserted");
	}
	public void CreateIndex(){
		
	
		collection.createIndex(new BasicDBObject("state","text"));
		collection.createIndex(new BasicDBObject("loc", "2dsphere") );
	}
	public void DeleteAllDocs(){
		
		
		collection.remove(new BasicDBObject());
		System.out.println("Deleted");
		
	}
	
	public String[] findNearBy(double longitude, double latitude){
		BasicDBList myLocation = new BasicDBList();
		myLocation.put(0, longitude);
		myLocation.put(1, latitude);
		 DBCursor cursor = collection.find(
		            new BasicDBObject("loc",
		                new BasicDBObject("$near",
		                        new BasicDBObject("$geometry",
		                                new BasicDBObject("type", "Point")
		                                    .append("coordinates", myLocation))
		                					.append("$maxDistance",  5000)
		                            
		                        )
		                )
		            );
		 Map<String,Integer> nearby=new HashMap<String,Integer>();
		 
		 List<String> nearByCordinates=new ArrayList<String>();
		 
		 while(true){
		 	if(cursor.hasNext()) {
				DBObject myDoc = cursor.next();
				String key=myDoc.get("state").toString();
				
				BasicDBObject location = (BasicDBObject)myDoc.get("loc");
				BasicDBList cordinates= (BasicDBList) location.get("coordinates");
				String lng =cordinates.get(0).toString();
				String lat =cordinates.get(1).toString();
				nearByCordinates.add(lng+"$#$#"+lat+"$#$#"+myDoc.get("name").toString()+"$#$#"+myDoc.get("city").toString()+"$#$#" + myDoc.get("stars").toString());
				if(nearby.containsKey(key))
					nearby.put(key, nearby.get(key) + 1);
				else
					nearby.put(key,  1);
					
			}
		 	else
		 		break;
		 }
		 	
		 
		    
		 for (String key : nearby.keySet()) {
			  System.out.println("State :"+key +"  No of Locaitons :"+ nearby.get(key));
			}
		 cursor.close();
		 System.out.println("Locations Found");
		 
		 String[] seedarray = new String[nearByCordinates.size()];
		    return nearByCordinates.toArray(seedarray);
	}
}
