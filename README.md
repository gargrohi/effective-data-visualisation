# Visualisation of Yelp Dataset with d3.js and leaflet.js #

The aim of this report is to make logical grouping of Yelp data with differnt d3.js view patterns. Also it allows users to search nearby hotels
on the basis of hotel category and view it through leaflet.js.

### What is this repository for? ###

* Visualisation of Yelp Dataset 
* Version: 1.0
* URL: bit.ly/Visuald3

### How do I get set up? ###

* SignUp and download Yelp DataSet
* Python script to process yelp dataset to form json compatable for d3
* Java API to interact with Mongodb to store spatial data.
* Named Entity recognition (Stanford Library) to get locations in seach query 
* Leaflet.js to plot the hotels on map depending upon the user location like "3 star hotels in Las Vegas" 
* Deployment instructions: Deploy war file in Tomcat Apache 

### Contribution  ###

* Yelp DataSet
* openstreetmap.org (integeration with leaflet api)

### Who do I talk to? ###

* Prof Sathyanaraya Raghavachary
* Yelp data team

* Visualizations

1. Selecting the state  
   ![1.png](https://bitbucket.org/repo/45RgME/images/2882431338-1.png)

2. Selecting the city
   ![1.png](https://bitbucket.org/repo/45RgME/images/18824551-1.png)

3. Selecting the hotel category
   ![1.png](https://bitbucket.org/repo/45RgME/images/2841372988-1.png) 

4. Selecting the hotel in the chosen category 
   ![1.png](https://bitbucket.org/repo/45RgME/images/2542073713-1.png)

5. Map visualization on the basis of user query
   ![1.png](https://bitbucket.org/repo/45RgME/images/1824017186-1.png)