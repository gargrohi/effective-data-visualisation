<%@page import="com.LoadData.UploadData"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>GEO Testing</title>
  <!-- include this stylesheet in your <head> -->
	<link href="http://cdn.leafletjs.com/leaflet-0.6.4/leaflet.css" rel="stylesheet" />
 
<!-- And the oldIE version of course. Just think, one day we'll no longer have to do this! -->
<!--[if lte IE 8]>
	<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.6.4/leaflet.ie.css" />
<![endif]-->
 
<!-- then include the script file. if you're not sure where to include it, just before the </body> is probably a good choice -->
<script type="text/javascript" src="http://cdn.leafletjs.com/leaflet-0.6.4/leaflet.js"></script>


<style type="text/css"><!--
#leafletmap{height: 600px;}
--></style>
</head>

<body>



  <h1>GEO Data</h1>
  <div id="leafletmap"></div>

 <script type="text/javascript">
 
  var mymap = L.map('leafletmap', {
	center: [[40.775,-73.972]],
	scrollWheelZoom: false,
	inertia: true,
	inertiaDeceleration: 2000
	});
  
 
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
attribution: 'Map data � <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
maxZoom: 18
}).addTo(mymap);

L.marker([25.77516, -80.2002]).addTo(mymap).bindPopup("<b>I am in Miami</b>");

var allText =[];


var txtFile = new XMLHttpRequest();
txtFile.open("GET", "data.txt", true);
var initial_lng=0;
var initial_lat=0;

var star5Icon = L.icon({
    iconUrl: 'http://stephenchastain.com/store/images/icons/3star.png',
    iconSize:     [40, 20], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor

    
});
var star1Icon = L.icon({
    iconUrl: 'http://www.explorelogancounty.com/css/icons/GoldStar.png',
    iconSize:     [40, 20], // size of the icon
    shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor

    
});

txtFile.onreadystatechange = function() 
{
	if (txtFile.readyState==4 && txtFile.status==200){
			allText = txtFile.responseText.split("\n");
			for (var i in allText) {
				//console.log(allText[i]);
				
				try {
					var lng=allText[i].split("$#$#")[0];
					var lat=allText[i].split("$#$#")[1];
					if(initial_lng==0){
						initial_lng=lng;
						initial_lat=lat;
					}
					if(initial_lng!=0)
						mymap.setView([parseFloat(initial_lat),parseFloat(initial_lng)], 12);
					else
						 mymap.setView([40.775,-73.972], 2);
						
					
					var hotelName=allText[i].split("$#$#")[2];
					var city=allText[i].split("$#$#")[3];
					var stars=allText[i].split("$#$#")[4];
					if(parseFloat(stars)==parseFloat(3.0)){
						L.marker([parseFloat(lat),parseFloat(lng)],{icon: star5Icon}).addTo(mymap).bindPopup("<b>Hotel: "+hotelName+"</b><br/>"+"<b>City: "+city+"</b><br/>"+"<b>Rating: "+stars+"</b><br/>");
					}
					else if(parseFloat(stars)==parseFloat(1.0)){
						L.marker([parseFloat(lat),parseFloat(lng)],{icon: star1Icon}).addTo(mymap).bindPopup("<b>Hotel: "+hotelName+"</b><br/>"+"<b>City: "+city+"</b><br/>"+"<b>Rating: "+stars+"</b><br/>");
					}
					else
						L.marker([parseFloat(lat),parseFloat(lng)]).addTo(mymap).bindPopup("<b>Hotel: "+hotelName+"</b><br/>"+"<b>City: "+city+"</b><br/>"+"<b>Rating: "+stars+"</b><br/>");
					
				}
				catch(err) {
				    alert(allText[i]);
				   
				}
				
				
				}
	}

}

txtFile.send();




 </script>
</body>

</html>