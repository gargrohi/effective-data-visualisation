package com.LoadData;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;

import org.json.JSONObject;

import com.DataAccess.Util.GoogleCordinates;
import com.MongoDBConn.MongoDBConnection;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;


public class UploadData  {
    private static final long serialVersionUID = 1L;
	
	private static final String GOOGLE_API_URL="https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s";
	private static final String API_KEY="AIzaSyDWYMb7rlmjfD9WluinbziExclUMcMpcYY";

       public static DBObject[] readData(String file){
    	   List<DBObject> seedList=new ArrayList<DBObject>();
    	   int counter=0;
    	   try (BufferedReader br = new BufferedReader(new FileReader(file))) {
    		    String line;
    		    
    		    while ((line = br.readLine()) != null) {
    		    	counter++;
    		    	Object obj = com.mongodb.util.JSON.parse(line);
    		    	   DBObject dbObj = (DBObject) obj;
    		    	   String longitude=dbObj.get("longitude").toString();
    		    	   String latitude=dbObj.get("latitude").toString();
    		    	   BasicDBList coordinates = new BasicDBList();
    		    	   coordinates.put(0, new Float(longitude).floatValue());
    		    	   coordinates.put(1, new Float(latitude).floatValue());
    		    	   dbObj.removeField("longitude");
    		    	   dbObj.removeField("latitude");
    		    	   dbObj.put("loc",new BasicDBObject("type", "Point").append("coordinates", coordinates));
    		    	   seedList.add(dbObj);
    		    }
    		    DBObject[] seedarray = new DBObject[seedList.size()];
    		    return seedList.toArray(seedarray);
    		} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
    	   return null;
       }
       
       public  String[] getData(String location){
    	  
    	   String []data=null;
    	   try{
    		   
    		   MongoDBConnection mongoDBConnection=new MongoDBConnection();
    		   String response=GoogleCordinates.getApiResponse(String.format(GOOGLE_API_URL,location,API_KEY));
    		   JSONObject json=  new JSONObject(response);
    		   if ( json.get("results")!=null && json.getJSONArray("results").length()>0 ) {
    			   JSONObject loc=json.getJSONArray("results").getJSONObject(0);
    			   if(loc!=null){
    				   double lng=loc.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
    				   double lat=loc.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
    				   data= mongoDBConnection.findNearBy(lng,lat);
    			   }
    		   }
    		   PrintWriter writer = new PrintWriter("C:\\Users\\Rohit\\Shopzilla\\javaProj\\DataFiltering\\WebContent\\data.html", "UTF-8");
    		   for (String line : data)
    			   writer.println(line);
        	   
        	   writer.close();
    	   }
    	  
    	   catch(Exception e){
    		   e.printStackTrace();
    	   }
    	  
    	   
		return data;
    	   
       }
       public static void main(String[] args){
		   MongoDBConnection mongoDBConnection=new MongoDBConnection();
		   mongoDBConnection.DeleteAllDocs();
    	   mongoDBConnection.insertData(readData("C:\\Users\\Rohit\\Desktop\\jqueryPlugin\\yelp_dataset_challenge_academic_dataset\\yelp_dataset_challenge_academic_dataset\\yelp_academic_dataset_business.json"));
    	   mongoDBConnection.CreateIndex();
    	   UploadData obj=new  UploadData();
    	   obj.getData("lasvegas");
       }
       
    
      
}
